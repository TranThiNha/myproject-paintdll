Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com
1. Các chức năng đã làm được:
- Cải tiến ứng dụng vẽ hình, đưa 2 hàm vào DLL và gọi lại từ ứng dụng chính:
hàm vẽ hình chữ nhật và hàm vẽ hình tròn
2. Luồng sự kiện chính:
- Hiện màn hình có menu Shape.
- Chọn loại hình cần vẽ trong menu Shape.
- Đè chuột trái và di chuyển để vẽ hình. Buông chuột để có được hình cuối cùng.
- Tiếp tục chọn các loại hình khác trong menu và vẽ tương tự.
- Giữ phím Shift khi đang vẽ hình chữ nhật sẽ vẽ ra hình vuông.
- Giữ phím Shift khi đang vẽ hình ellipse sẽ vẽ ra hình tròn. 
3. Luồng sự kiện phụ:
- Mặc định cho người dùng vẽ đường thẳng khi vừa mở màn hình lên.
- Có thể vẽ nhiều loại hình trên màn hình mà không mất đi hình cũ.
4. Link Bitbucket: https://bitbucket.org/TranThiNha/myproject-paintdll
5. Link Youtube: https://youtu.be/o9DvYD_Rdbs