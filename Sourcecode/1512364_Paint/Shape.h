#pragma once
class CShape
{
protected:
	int x1;
	int y1;
	int x2;
	int y2;
public:
	CShape();
	virtual void Draw(HDC hdc) = 0;
	void SetData(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	virtual CShape* Create(int a, int b, int c, int d) = 0;
	~CShape();
};

