#pragma once
#include "Shape.h"
#include "PaintLib.h"

class CCircle :
	public CShape
{
public:
	CCircle();
	~CCircle();
	CCircle(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(HDC hdc);

	CShape* Create(int a, int b, int c, int d) {
		return new CCircle(a, b, c, d);
	}
};

