#pragma once
#include "Shape.h"
#include "PaintLib.h"

class CRectangle :
	public CShape
{
public:
	CRectangle();
	~CRectangle();
	CRectangle(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(HDC hdc);

	CShape* Create(int a, int b, int c, int d) {
		return new CRectangle(a, b, c, d);
	}

};

