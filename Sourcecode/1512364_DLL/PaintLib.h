#pragma once
#include <math.h>

#ifdef PAINTLIBRARY_EXPORTS
#define PAINTLIBRARY_API __declspec(dllexport) 
#else
#define PAINTLIBRARY_API __declspec(dllimport) 
#endif

namespace PaintDll
{
	class DrawDLL
	{
	public:
	static	PAINTLIBRARY_API void DrawDLLRec(HDC, int , int , int , int );
	static	PAINTLIBRARY_API void DrawDLLCircle(HDC , int , int , int , int );
	};
}

