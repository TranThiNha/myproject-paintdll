#include "stdafx.h"
#include "PaintLib.h"

namespace PaintDll
{
	void DrawDLL::DrawDLLRec(HDC hdc, int a, int b, int c, int d)
	{
		Rectangle(hdc, a, b, c, d);
	}

	void DrawDLL::DrawDLLCircle(HDC hdc, int x1, int y1, int x2, int y2)
	{
		int min;
		if (abs(x2 - x1) > abs(y2 - y1))
		{
			min = abs(x2 - x1);
		}
		else min = abs(y2 - y1);
		if (x2 > x1 && y2 > y1)
			Ellipse(hdc, x1, y1, x1+min, y1+min);
		else if (x2 > x1 && y2 < y1)
			Ellipse(hdc, x1, y2, x1 + min, y2 + min);
		else if (x2 < x1 && y2 < y1)
			Ellipse(hdc, x2, y2, x2 + min, y2 + min);
		else
			Ellipse(hdc, x2, y1, x2 + min, y1 + min);
	}
}
